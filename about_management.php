<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body  bgcolor="#b3ffe8">
	<?php
		include 'navi.php';
	?>

	<div class="tinted-image" width = "100%" style="border:1px solid black;">

		<h1 style="text-align:center;">More About us</h1>

		<span style="text-align:center; font-size: 18px;">
			<p>
				<u>SELECTION OF RECIPIENT</u><br>
				We Will be selecting to distribute food vouchers randomly where the pooor people gather (churches, temples etc). We will be going in person and handing them over food Vouchers. Then they can present the Voucher to any Kells / Food City and select food according to the value of the voucher.<br><br>

				<u>ORGANIZATIONAL STRUCTURE OF THE FREE FOOD VOUCHER PROJECT</u><br>
				We have to find permanent small to medium size to store the food and also use as a distribution Center both.  We can start from Colombo where you have more poor People and gradually extend to all the Major Cities and small Cities both.<br><br>

				<u>WE ONLY DISTRIBUTE FOOD VOUCHERS AND NOT FOOD</u><br>
				We will be distributing Food Vouchers only and the recipient can get what they want within the value of the Food Voucher<br><br>

				<u>MANAGEMENT AND WORKERS</u><br>
				Open to People who believe in this Project to volunteer to help and others.<br><br>
				 
				<u>OPERATIONAL DATE</u><br>
				We will be distributing once a month.<br><br>

				<u>WORKING HOURS</u><br>
				We are open 24/7 to recieve your calls. For contact information click <a href="contact.php"> here </a>.<br><br>

				<u>RECRUITING OF WORKERS TO PROMOTE SAHANAYA</u><br>
				We will appeal to all Religious leaders to send their followers in TEMPLES AND CHURCHES.  And open to Companies, Universities and Colleges, and the general public to join and help a worthy cause.<br><br>
			</p>
		</span>

	</div>

	<?php
		include 'footer.php';
	?>


</body>
</html>