<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body  bgcolor="#b3ffe8">
	<?php
		include 'navi.php';
	?>

	<h1 style="text-align:center;"><u>CONTACT INFORMATION</u></h1>

	<h2>Please Contact</h2>
	<ul>
		<li>
			<h3>Mrs. Damayanthi Wijegoonawardane (CEO SAHANAYA)</h3>
			<ul>
				<li>Tel - +94 71 777 6365</li>
				<li>Email - contact@sahanayafood.com</li>
			</ul>
		</li>
		<li>
			<h3>Mr. Anura Wijemanna (Vice President SAHANAYA)</h3>
			<ul>
				<li>Tel - +94 77 763 1877</li>
				<li>Email - contact@sahanayafood.com</li>
			</ul>
		</li>
		<li>
			<h3>Mr. Tyrrel Ignatius (Founding President SAHANAYA)</h3>
			<ul>
				<li>Tel - +1 437 3450 688 (Toronto, Canada)</li>
				<li>Email - contact@sahanayafood.com</li>
			</ul>
		</li>
		<li>
			<h3>Mr. Pasan Perera (Web master SAHANAYA) - For tehnical support</h3>
			<ul>
				<li>Tel - +94 72 545 0951 (Colombo, Sri Lanka)</li>
				<li>Email - webmaster@sahanayafood.com</li>
			</ul>
		</li>
	</ul>

	<?php
		include 'footer.php';
	?>

</body>
</html>