<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body  bgcolor="#b3ffe8">
	<?php
		include 'navi.php';
	?>

	<h1 style="text-align:center;">LANKA FOOD RELIEF</h1>
	<h2 style="text-align:center;">Top 10 Facts about Hunger in Sri Lanka</h2>

	<ol>
		<li>Despite reducing maternal mortality rates and cutting poverty in half, Sri Lanka is still struggling with food insecurity. Undernutrition of its population remains a prevalent issue in Sri Lanka. While the country has been making big strides, food insecurity has the ability to get in the way of socio-economic development </li>
		<li>Sri Lanka’s levels of chronic malnutrition, or stunting, are the lowest in the region at 13 percent. Compared to its peers (India has an alarming rate of 38 percent), this rate is not high. Stunting prevents proper brain and body development in children, leading to a less prosperous and healthy population.</li>
		<li>However, Sri Lanka has one of the highest rates of acute malnutrition and micronutrient deficiencies in the world. Just behind Djibouti and South Sudan, Sri Lanka ranks third with rates between 14 and 35 percent throughout the country’s districts, which is particularly concerning for the health of Sri Lanka’s population?</li>
		<li>Sri Lanka has a 19.6 percent prevalence of wasting. Wasting refers to a low body weight compared to height ratio, illustrating the considerable effect of undernutrition. </li>
		<li>Sri Lanka’s island status makes it vulnerable to unpredictable weather patterns. Since 2016, Sri Lanka has been suffering from a severe drought that is still affecting 1.2 million people across the country. This significantly contributes to food insecurity because it wipes out large amounts of crops and agricultural food sources.  </li>
		<li>The government of Sri Lanka estimates that 480,000 food insecure people will need humanitarian aid because of the effects of the 2016 drought. This aid is necessary to prevent the hunger problem from getting worse.    </li>
		<li>As of 2017, the Sri Lankan government announced its plan to prioritize ending hunger and malnutrition. Working with the World Food Programme, the government is focusing on achieving Sustainable Development Goal 2. This goal includes improving food security, ending hunger and advancing sustainable agriculture by 2030.</li>
		<li>Rice production has dropped severely since the drought-hit Sri Lanka. A rice harvest in 2017 yielded 63 percent below normal. The population depends greatly on rice for sustenance and survival, and it is the main item affected by the drough </li>
		<li>Among developing countries, Sri Lanka ranked 87th on the Global Hunger Index. This list was calculated by taking into account rates of stunting and wasting in children under 5, the amount of population undernourished, and infant mortality rates. From “Low” to “Extremely Alarming,” this gave the country status of “Serious” in 2016.    </li>
		<li>Among developing countries, Sri Lanka ranked 87th on the Global Hunger Index. This list was calculated by taking into account rates of stunting and wasting in children under 5, the amount of population undernourished, and infant mortality rates. From “Low” to “Extremely Alarming,” this gave the country status of “Serious” in 2016.</li>
	</ol>

	<h3 style="text-align:center;">Although Sri Lanka is improving in many ways, the country has a long way to come in regards to food insecurity. These top 10 facts about hunger in Sri Lanka provide clear insight into the humanitarian efforts that need to be made to mitigate this issue.</h3>
	<h3 style="text-align:center;">– Amelia Merchant <br>E HUB in the 2019</h3>

	<?php
		include 'footer.php';
	?>

</body>
</html>