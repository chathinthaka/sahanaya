<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body bgcolor="#b3ffe8">

	
	<?php
		include 'navi.php';
	?>

	<div style="background-color:whitesmoke;">
		<h2 style="color: #f05454; text-align: center;">WORLD'S PREMIER SRI LANKA SAHANAYA FOOD FOR THE NEEDY WELCOMES YOU.</h2>
	</div>

	<br>

	<table width="80%" align="Center">
		<tr>
			<td width="65%">
				<span style="text-align: right; vertical-align: top;padding-right: 30px;">
					<h3 style="color: darkcyan;"><u>MISSION STATEMENT</u></h3>
					<h2>“Make hunger in Lanka a thing in the past by collectively distributing dry ration Food Vouchers regularly to the needy”</h2>
				</span>
				<div class="below_mission" style="border:1px solid black;text-align: center;">
					<span id="text_after_mission" style="padding-top: 5px;">
						<h2 style="color: blue; text-transform: uppercase;padding-bottom: 2px;padding-top: 1px;"><u>WORLD'S PREMIER SRI LANKA Food Vouchers for the needy</u></h2>
						<h4 style="color: blue; text-transform: uppercase;"><u>Distribution of Food Vouchers to the needy</u></h4>
						<h2>We are handing over Food Vouchers to the needy and not dry rations. This will help the recipient to 
						buy what ever he/she need.<br>
						And also it will avoid us the trouble of packaging and distributing.
						This is the first Food Voucher Food Bank in Sri-Lanka.<br>
						The value of the Food voucher will depend on the monthly donations we recieve from you.(For example it can vary from Rs.2000 to Rs.5000 each)</h2>
					</span>
				</div>
			</td>
			<td width="2%"></td>
			<td width="33%">
				<img src="images/Community Service.webp" width="100%">
			</td>
		</tr>
	</table>

	<br><br>

	<span style="color: red;">
		<h1 align="Center" style="color: black;">
		<u>YOUR CONTRIBUTIONS</u>
	</span><br></h1>
	<div style="padding: 10px 50px; border: 2px solid; border-color: blue; margin: 30px;">


		<h3>
		Your Big or Small donation will assure the success of this much needed PREMIER
		PERMANENT FOOD BANK (SAHANAYA – which means RELIEF in English) in Sri-
		Lanka, to distribute Food Vouchers. At present we are only looking for - FOOD VOUCHERS AND NOT ANY
		CASH DONATIONS, to avoid Administration/Registration issues.
		<br><br>		
		So as such please send us your donations directly to buy FOOD VOUCHERS to
		(KEELS FOOD / FOOD CITY, Colombo Locations, Sri-Lanka. Tele +94 112 303 111.
		Web https:keelssuper.com). Thanks for your continues contribution for joining us to DISTRIBUTE
		FREE FOOD VOUCHERS FOR THE NEEDY.
		<br><br>
		PLESE-NOTE - 
		<br>
		<h2>OFFICIAL LAUNCH - SAHANAY PROJECT IS SCHEDULED TO OPEN TO COINSIDE WITH SINHALA / TAMIL NEW YEAR & EASTER - APRIL 2022, SUBJECT TO FUNDING</h2>
		<span style="color:blue;">Please email your donation amount send to –
			contact@sahanayafood.com, to KEELSFOOD/FOOD CITY; so that we can track the amount
			and give away food vouchers accordingly. Thanks for your kind contribution.
			<br><br>
			Beside blessings for your contribution to joining us, to feed the hungry in Sri-Lanka,
			you will be a part of history in the making to distribute Premier Free Food Vouchers in Sri-Lanka.
			<br><br>
			SAHANAYA FOOD VOUCHER OUTLET MODEL – It's a creation by Sahanaya Food Founding Partners in Sri Lanka.
			<br><br>
			We start with COLOMBO (Capital) then will extend to all other Provinces/Cities
			in the near future.
		</span>
		</h3>
	</div>

	
	<div style="background-color: whitesmoke;">

		<h1 style="color: green;text-align: center;">SAHANAYA WILL BE A PERMANENT FOOD VOUCHERS DISTRIBUTING CENTER, TO DISTRIBUTE FOOD VOUCHERS FOR THE NEEDY ON A MONTHLY BASIS</h1>

	</div>

	<img src="images/11.jpg" width="45.5%"height="400px"><img src="images/13.jpg" width="54.5%" height="400px">

	<div>
		<h3>What you can buy for the Sahanaya Food Vouchers and more</h3>
	</div>

	<img src="images/bestfood.jpg" width="50%">


	<?php
		include 'footer.php';
	?>

</body>
</html>