<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body  bgcolor="#b3ffe8">
	<?php
		include 'navi.php';
	?>

	<h1 style="text-align:center;">LONG TERM <span style="color:red;">"SAHANAYA - PROJECT"</span> PLANS</h1>

	<ul>
		<li>This is only a draft Proposal of the PROJECT IDEA. THE FINAL PROJECT WILL BE FINALIZED AFTER THE FEEDBACK WE GET FROM LIKE-MINDED PROFESSIONALS/FRIENDS.</li><br>
		<li>The ADMINISTRATION OPERATIONS WILL BE DONE SUMMIT FLAT OFFICE IN COLOMBO - 02.</li><br>
		<li>We will register as a non for profit and will appoint qualified INDIVIDUAL TO JOIN THE BOARD WITH A HEART AND BRAINS both.</li><br>
		<li>We hope to start from Colombo (capital) , gradually will be opening in other major Cities/Provinces.</li><br>
		<li>We are going for funding for the Sahanaya project from locals in Sri Lanka and out of Sri Lanka. We would be delighted if you can promote the Sahanaya project to anyone who can help by way of contribution for this much needed Sahanaya Food Voucher project.</li><br>
	</ul>

	<?php
		include 'footer.php';
	?>

</body>
</html>